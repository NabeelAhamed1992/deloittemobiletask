package com.nabeel.deloitte.clothstore.viewmodel;

import android.util.Log;
import android.view.View;

import androidx.databinding.ObservableDouble;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nabeel.deloitte.clothstore.R;
import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.net.Constants;
import com.nabeel.deloitte.clothstore.productoperations.ProductOperationListener;
import com.nabeel.deloitte.clothstore.productoperations.ProductOperations;
import com.nabeel.deloitte.clothstore.ui.WishListtAdapter;

import java.util.ArrayList;
import java.util.List;

public class WishListViewModel  extends ViewModel {


    private WishListtAdapter adapter;
    public MutableLiveData<Product> selected;

    private MutableLiveData<ArrayList<Product>> wishListItems;

    public ObservableDouble totalPriceInt;
    public MutableLiveData<Integer> showProgressStatus;
    public MutableLiveData<String> toastMsgLiveData;

    public void init()
    {
        wishListItems = new MutableLiveData<>();
        selected = new MutableLiveData<>();
        adapter = new WishListtAdapter(R.layout.wishlist_view_product, this);
        totalPriceInt = new ObservableDouble(0);
        showProgressStatus =new MutableLiveData();
        showProgressStatus.setValue(View.GONE);
        toastMsgLiveData = new MediatorLiveData<>();

    }




    public void setUpWishListems() {

        wishListItems.setValue(ProductOperations.getInstance().getWishListItems());

    }
    public MutableLiveData<ArrayList<Product>> getWishListItems(){
        return wishListItems;
    }

    public MutableLiveData<ArrayList<Product>> getCartItems() { return wishListItems;
    }

    public void setAdapter(List<Product> products)
    {
        this.adapter.setProducts(products);
        this.adapter.notifyDataSetChanged();
    }

    public WishListtAdapter getAdapter()
    {
        return this.adapter;
    }


    public void addItemToCart(Integer index)
    {




        showProgressStatus.setValue(View.VISIBLE);
        Product product =  ProductOperations.getInstance().getWishListItems().get(index);
        ProductOperations.getInstance().addToCart(product, new ProductOperationListener() {
            @Override
            public void onProductRemovedFromCart(Product product) {

            }

            @Override
            public void onProductAddedToCart(Product product) {
                showProgressStatus.setValue(View.GONE);
                ProductOperations.getInstance().removeFromWishList(product);
                setUpWishListems();
            }

            @Override
            public void onProductAddedToWishList(Product product) {

            }

            @Override
            public void onFetchedAllProducts(ArrayList<Product> products) {

            }

            @Override
            public void onError(String message) {
                showProgressStatus.setValue(View.GONE);
                if(message.equalsIgnoreCase(Constants.SHOW_MSG_ALREADY_ADDED)){
                    toastMsgLiveData.setValue(Constants.SHOW_MSG_ALREADY_ADDED);
                    return;
                }
                toastMsgLiveData.setValue(Constants.SHOW_MSG_MOVE_ITEM_CART_FAILED);
            }
        });






    }



    public int IsToShowCartIcon(int index)
    {
        return getProductAt(index).isOutOfStock == View.GONE ?View.VISIBLE :View.GONE;
    }
    public Product getProductAt(Integer index) {
        if (wishListItems.getValue() != null &&
                index != null &&
                wishListItems.getValue().size() > index) {
            return wishListItems.getValue().get(index);
        }
        return null;
    }


}
