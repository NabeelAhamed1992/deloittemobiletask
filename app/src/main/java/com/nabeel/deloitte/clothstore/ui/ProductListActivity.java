package com.nabeel.deloitte.clothstore.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.nabeel.deloitte.clothstore.R;
import com.nabeel.deloitte.clothstore.databinding.ActivityProductListBinding;
import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.viewmodel.ProductListViewModel;

import java.util.List;

public class ProductListActivity extends AppCompatActivity {
    private ProductListViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product_list);
        getSupportActionBar().hide();
        setupBindings(savedInstanceState);

    }
    ActivityProductListBinding activityProductListBinding;
    private void setupBindings(Bundle savedInstanceState)
    {
       activityProductListBinding =  DataBindingUtil.setContentView(this, R.layout.activity_product_list);
        viewModel = ViewModelProviders.of(this).get(ProductListViewModel.class);
        if (savedInstanceState == null) {
            viewModel.init();
        }
        activityProductListBinding.setModel(viewModel);
        activityProductListBinding.setLifecycleOwner(this);
        setupListUpdate();


        setProgressDialog();

        setupToast();
    }

    private void setupToast() {
        viewModel.toastMsgLiveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(ProductListActivity.this, s,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupListUpdate() {
        viewModel.loading.set(View.VISIBLE);
        viewModel.fetchList();
        viewModel.getProducts().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {
                viewModel.loading.set(View.GONE);
                if (products.size() == 0) {
                    viewModel.showEmpty.set(View.VISIBLE);
                } else {
                    viewModel.showEmpty.set(View.GONE);
                    viewModel.setAdapter(products);
                }
            }
        });
        setupListClick();
    }



    Dialog progressDailog;
    void setProgressDialog()
    {
        viewModel.showProgressStatus.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

                if(integer == View.VISIBLE)
                {

                    progressDailog = new Dialog(ProductListActivity.this);
                    progressDailog.setContentView(R.layout.progress_dialog);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(progressDailog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;


                    progressDailog.show();
                    progressDailog.getWindow().setAttributes(lp);


                }
                if(integer == View.GONE)
                {

                    if(progressDailog!=null)
                    progressDailog.dismiss();


                }

            }
        });
    }



    private void setupListClick() {
        viewModel.getSelected().observe(this, new Observer<Product>() {
            @Override
            public void onChanged(Product product) {
                if (product != null) {
                    Toast.makeText(ProductListActivity.this, "You selected a " +product.getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();




    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        Log.e("RESTART", "CART ITEMS");
        viewModel.updateCartNotification();
        viewModel.updateWishListNotification();


    }
}
