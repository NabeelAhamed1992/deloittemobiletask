package com.nabeel.deloitte.clothstore.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.nabeel.deloitte.clothstore.R;
import com.nabeel.deloitte.clothstore.databinding.ActivityShopCartBinding;
import com.nabeel.deloitte.clothstore.databinding.ActivityWishListBinding;
import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.viewmodel.ShopCartViewModel;
import com.nabeel.deloitte.clothstore.viewmodel.WishListViewModel;

import java.util.ArrayList;

public class WishListActivity extends AppCompatActivity {

    WishListViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        setTitle(R.string.wishlist);
        setupBindings(savedInstanceState);

    }

    private void setupBindings(Bundle savedInstanceState)
    {
         ActivityWishListBinding wishListBinding = DataBindingUtil.setContentView(this, R.layout.activity_wish_list);
        viewModel = ViewModelProviders.of(this).get(WishListViewModel.class);
        if (savedInstanceState == null)
        {
            viewModel.init();
        }
        wishListBinding.setModel(viewModel);
        wishListBinding.setLifecycleOwner(this);
        setupListUpdate();
        setProgressDialog();
        setupToastUpdate();


    }

    private void setupToastUpdate() {
        viewModel.toastMsgLiveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(WishListActivity.this, s,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupListUpdate()
    {

        viewModel.setUpWishListems();


        viewModel.getWishListItems().observe(this, new Observer<ArrayList<Product>>() {
            @Override
            public void onChanged(ArrayList<Product> products) {

                if(products.size() == 0)
                {
                    Toast.makeText(WishListActivity.this, "WishList Items Empty", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    viewModel.setAdapter(products);
                }
            }
        });

    }

    Dialog progressDailog;
    void setProgressDialog()
    {
        viewModel.showProgressStatus.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

                if(integer == View.VISIBLE)
                {

                    progressDailog = new Dialog(WishListActivity.this);
                    progressDailog.setContentView(R.layout.progress_dialog);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(progressDailog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;


                    progressDailog.show();
                  //  progressDailog.getWindow().setAttributes(lp);


                }
                if(integer == View.GONE)
                {

                    if(progressDailog!=null)
                        progressDailog.dismiss();


                }

            }
        });
    }
}
