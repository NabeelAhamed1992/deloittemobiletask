package com.nabeel.deloitte.clothstore.viewmodel;

import androidx.databinding.ObservableDouble;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nabeel.deloitte.clothstore.R;
import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.net.Constants;
import com.nabeel.deloitte.clothstore.productoperations.ProductOperationListener;
import com.nabeel.deloitte.clothstore.productoperations.ProductOperations;
import com.nabeel.deloitte.clothstore.ui.ShopCartListtAdapter;

import java.util.ArrayList;
import java.util.List;

public class ShopCartViewModel extends ViewModel {


    private ShopCartListtAdapter adapter;
    public MutableLiveData<Product> selected;

    private MutableLiveData<ArrayList<Product>> cartItems;

    public ObservableDouble totalPriceInt;

    public MutableLiveData<String> toastMsgLiveData;



    public void init()
    {

        cartItems = new MutableLiveData<>();

        selected = new MutableLiveData<>();
        adapter = new ShopCartListtAdapter(R.layout.cart_view_product, this);
        totalPriceInt = new ObservableDouble(0);
        toastMsgLiveData = new MediatorLiveData<>();



    }




    public void setUpCartItems() {
        cartItems.setValue(ProductOperations.getInstance().getCartItems());
    }
    public MutableLiveData<ArrayList<Product>> getCartItems() { return cartItems;
    }

    public void setAdapter(List<Product> products) {
        this.adapter.setProducts(products);
        this.adapter.notifyDataSetChanged();
    }

    public ShopCartListtAdapter getAdapter()
    {
        return this.adapter;
    }

    public void removeItemFromCart(Integer index)
    {


      ProductOperations.getInstance().removeFromCart(cartItems.getValue().get(index), new ProductOperationListener() {
          @Override
          public void onProductRemovedFromCart(Product product) {

              setUpCartItems();
              updateTotalPrice();
          }

          @Override
          public void onProductAddedToCart(Product product) {



          }

          @Override
          public void onProductAddedToWishList(Product product) {

          }

          @Override
          public void onFetchedAllProducts(ArrayList<Product> products) {

          }

          @Override
          public void onError(String message) {

              toastMsgLiveData.setValue(Constants.SHOW_MSG_REMOVE_ITEM_FROM_CART_FAILED);

          }
      });




    }

    public void updateTotalPrice()
    {

        totalPriceInt.set(ProductOperations.getInstance().getTotalAmount());



    }


    public Product getProductAt(Integer index) {
        if (cartItems.getValue() != null &&
                index != null &&
                cartItems.getValue().size() > index) {
            return cartItems.getValue().get(index);
        }
        return null;
    }


}

