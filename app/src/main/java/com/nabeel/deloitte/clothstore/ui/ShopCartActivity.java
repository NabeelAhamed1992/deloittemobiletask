package com.nabeel.deloitte.clothstore.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.widget.Toast;

import com.nabeel.deloitte.clothstore.R;
import com.nabeel.deloitte.clothstore.databinding.ActivityShopCartBinding;
import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.viewmodel.ShopCartViewModel;

import java.util.ArrayList;

public class ShopCartActivity extends AppCompatActivity {

    private ShopCartViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_cart);

        setTitle(R.string.cart_items);
    setupBindings(savedInstanceState);
    setupToast();

  }

    private void setupToast() {
        viewModel.toastMsgLiveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(ShopCartActivity.this, s,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupBindings(Bundle savedInstanceState)
    {
        ActivityShopCartBinding activityProductListBinding  =  DataBindingUtil.setContentView(this, R.layout.activity_shop_cart);
        viewModel = ViewModelProviders.of(this).get(ShopCartViewModel.class);
        if (savedInstanceState == null) {
            viewModel.init();
        }
        activityProductListBinding.setModel(viewModel);
        activityProductListBinding.setLifecycleOwner(this);
        setupListUpdate();



    }

    private void setupListUpdate()
    {

        viewModel.setUpCartItems();
        viewModel.updateTotalPrice();

        viewModel.getCartItems().observe(this, new Observer<ArrayList<Product>>() {
            @Override
            public void onChanged(ArrayList<Product> products) {

                if(products.size() == 0)
                {
                    Toast.makeText(ShopCartActivity.this, "Cart Items Empty", Toast.LENGTH_SHORT).show();

                    finish();
                }
                else
                {
                    viewModel.setAdapter(products);
                }
            }
        });

    }
}
