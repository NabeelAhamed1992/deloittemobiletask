package com.nabeel.deloitte.clothstore.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;


import com.nabeel.deloitte.clothstore.BR;
import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.viewmodel.ProductListViewModel;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.GenericViewHolder> {

    private int layoutId;
    private List<Product> products;
    private ProductListViewModel viewModel;

    public ProductListAdapter(@LayoutRes int layoutId, ProductListViewModel viewModel) {
        this.layoutId = layoutId;
        this.viewModel = viewModel;
    }

    private int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount()
    {
        return products == null ? 0 : products.size();
    }

    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);

        return new GenericViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
        holder.bind(viewModel, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    class GenericViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        GenericViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ProductListViewModel viewModel, Integer position) {

            binding.setVariable(BR.viewModel, viewModel);
            binding.setVariable(BR.position, position);

            binding.executePendingBindings();
        }

    }
}
