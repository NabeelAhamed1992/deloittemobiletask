package com.nabeel.deloitte.clothstore.viewmodel;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.databinding.ObservableInt;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nabeel.deloitte.clothstore.R;
import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.net.Constants;
import com.nabeel.deloitte.clothstore.productoperations.ProductOperationListener;
import com.nabeel.deloitte.clothstore.productoperations.ProductOperations;
import com.nabeel.deloitte.clothstore.ui.ProductListAdapter;
import com.nabeel.deloitte.clothstore.ui.ShopCartActivity;
import com.nabeel.deloitte.clothstore.ui.WishListActivity;

import java.util.ArrayList;
import java.util.List;

public class ProductListViewModel extends ViewModel {




    private ProductListAdapter adapter;
    public MutableLiveData<Product> selected;
    public ObservableInt loading;
    public ObservableInt showEmpty;

    public ObservableInt cardItemsNos;
    public ObservableInt isToShowCartItemNos;

    public ObservableInt wishItemsNos;
    public ObservableInt isToShowWishItemNos;

    public MutableLiveData<Integer> showProgressStatus;
    public MutableLiveData<String> toastMsgLiveData;

    public MutableLiveData<ArrayList<Product>> productItems;


    public void init()
    {
        productItems = new MutableLiveData<>();

        selected = new MutableLiveData<>();
        adapter = new ProductListAdapter(R.layout.view_product, this);
        loading = new ObservableInt(View.GONE);
        showEmpty = new ObservableInt(View.GONE);

        cardItemsNos =new ObservableInt(0);
        isToShowCartItemNos=new ObservableInt(View.GONE);

        wishItemsNos =new ObservableInt(0);
        isToShowWishItemNos=new ObservableInt(View.GONE);

        showProgressStatus =new MutableLiveData();
        showProgressStatus.setValue(View.GONE);

        toastMsgLiveData =  new MediatorLiveData<>();
    }

    public void fetchList() {
        ProductOperations.getInstance().fetchList(new ProductOperationListener() {
            @Override
            public void onProductRemovedFromCart(Product product) {

            }

            @Override
            public void onProductAddedToCart(Product product) {

            }

            @Override
            public void onProductAddedToWishList(Product product) {

            }

            @Override
            public void onFetchedAllProducts(ArrayList<Product> products)
            {

                productItems.setValue(products);
            }

            @Override
            public void onError(String message) {
                toastMsgLiveData.setValue(Constants.SHOW_MSG_FETCH_ITEM_FAILED);
                loading.set(View.GONE);
            }
        });
    }

    public MutableLiveData<ArrayList<Product>> getProducts() {
        return productItems;
    }

    public ProductListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(List<Product> products) {
        this.adapter.setProducts(products);
        this.adapter.notifyDataSetChanged();
    }

    public MutableLiveData<Product> getSelected() {
        return selected;
    }


    public void addItemToCart(Integer index) {
        Product db = getProductAt(index);
        showProgressStatus.setValue(View.VISIBLE);
        ProductOperations.getInstance().addToCart(db, new ProductOperationListener() {
            @Override
            public void onProductRemovedFromCart(Product product) {


            }

            @Override
            public void onProductAddedToCart(Product product) {
                showProgressStatus.setValue(View.GONE);

                updateCartNotification();

            }

            @Override
            public void onProductAddedToWishList(Product product) {

            }

            @Override
            public void onFetchedAllProducts(ArrayList<Product> products) {

            }

            @Override
            public void onError(String message) {

                showProgressStatus.setValue(View.GONE);
                if(message.equalsIgnoreCase(Constants.SHOW_MSG_ALREADY_ADDED)){
                    toastMsgLiveData.setValue(Constants.SHOW_MSG_ALREADY_ADDED);
                    return;
                }
                toastMsgLiveData.setValue(Constants.SHOW_MSG_ADD_ITEM_TO_CART);
            }
        });
    }
    public void addItemToWishList(Integer index) {
        Product db = getProductAt(index);
        ProductOperations.getInstance().addToWishList(db, new ProductOperationListener() {
            @Override
            public void onProductRemovedFromCart(Product product) {

            }

            @Override
            public void onProductAddedToCart(Product product) {

            }

            @Override
            public void onProductAddedToWishList(Product product) {
                updateWishListNotification();
            }

            @Override
            public void onFetchedAllProducts(ArrayList<Product> products) {

            }

            @Override
            public void onError(String message) {

                if(message.equalsIgnoreCase(Constants.SHOW_MSG_ALREADY_ADDED)){
                    toastMsgLiveData.setValue(Constants.SHOW_MSG_ALREADY_ADDED);
                    return;
                }
                toastMsgLiveData.setValue(Constants.SHOW_MSG_ADD_ITEM_TO_WISHLIST_FAILED);
            }
        });



    }

    public void updateCartNotification()
    {
        if(ProductOperations.getInstance().getCartItems().size() == 0)
        {
            isToShowCartItemNos.set(View.GONE);
        }
        else
        {
            isToShowCartItemNos.set(View.VISIBLE);
            cardItemsNos.set(ProductOperations.getInstance().getCartItems().size());
        }

    }
    public void updateWishListNotification()
    {
        if(ProductOperations.getInstance().getWishListItems().size() == 0)
        {
            isToShowWishItemNos.set(View.GONE);
        }
        else
        {
            isToShowWishItemNos.set(View.VISIBLE);
            wishItemsNos.set(ProductOperations.getInstance().getWishListItems().size());
        }

    }

    public void onClickShopCartActvity(View v)
    {
        Intent i  = new Intent(v.getContext(), ShopCartActivity.class);
        v.getContext().startActivity(i);

    }
    public void onClickWishListActivity(View v)
    {
        Intent i  = new Intent(v.getContext(), WishListActivity.class);
        v.getContext().startActivity(i);

    }


    public void categoryClick(int type)
    {

       ArrayList<Product> productArrayList =  ProductOperations.getInstance().getProductCategory(type);
       if(productArrayList != null && productArrayList.size() > 0)
       {
           productItems.setValue(productArrayList);
       }


    }
    public void cartClick()
    {
        Log.e("CART", "CLICK");

    }

    public Product getProductAt(Integer index) {
        if (productItems.getValue() != null &&
                index != null &&
                productItems.getValue().size() > index) {
            return productItems.getValue().get(index);
        }
        return null;
    }
    public int IsToShowCartIcon(int index)
    {
        return getProductAt(index).isOutOfStock == View.GONE ?View.VISIBLE :View.GONE;
    }










}
