package com.nabeel.deloitte.clothstore.net;

import android.content.Context;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;



import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by keerthini on 28/09/16.
 */

public class ServiceHelper {

    private JSONObject jsonResultText;
    private String TAG = "Clutto";

    public ServiceHelper() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


    }

    public Response doHttpUrlConnectionAction(String requestData, String requestURL, String requestType) throws Exception {
        URL url = null;
        Response responseObj = new Response();

        BufferedReader reader = null;
        StringBuilder stringBuilder;
       // Log.e(TAG, requestData + Constants.EMPTY_STRING + requestURL);
        try {
            // create the HttpURLConnection
            url = new URL(requestURL.replace(" ", "%20"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            if (requestType.equalsIgnoreCase(Constants.REQUEST_TYPE_POST)) {
                connection.setDoOutput(true);
            } else {
                connection.setDoOutput(false);
            }
            connection.setRequestMethod(requestType);
            connection.setReadTimeout(30 * 1000);
            connection.setRequestProperty(Constants.KEY_CONTENT_TYPE, Constants.VALUE_CONTENT_TYPE);
            connection.setRequestProperty(Constants.KEY_ACCEPT, Constants.VALUE_CONTENT_TYPE);
            connection.connect();
            if (requestType.equalsIgnoreCase(Constants.REQUEST_TYPE_POST)) {
                OutputStreamWriter streamWriter = new OutputStreamWriter(connection.getOutputStream());
                streamWriter.write(requestData);
                streamWriter.close();
            }
            // read the output from the server
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            Log.e("RES CODE", String.valueOf(connection.getResponseCode()));
            Log.e("RES MESSAGE", String.valueOf(connection.getResponseMessage()));

            logConnection(connection);
            responseObj.setResponseMessage(connection.getResponseMessage());
            responseObj.setStatusCode(connection.getResponseCode());
            responseObj.setUrl(requestURL);
            responseObj.setSuccess(true);

            stringBuilder = new StringBuilder("");

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            if (!TextUtils.isEmpty(stringBuilder.toString())) {
                responseObj.setResponseStr(stringBuilder.toString());
                return  responseObj;
            }

            //connection.
            return responseObj;
        }


        catch (Exception e)
        {
            e.printStackTrace();
            responseObj.setSuccess(false);

            return  responseObj;


        } finally
        {
            // close the reader; this can throw an exception too, so
            // wrap it in another try/catch block.
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

    /**
     * Logs connection information.
     *
     * @param connection Connection to be logged.
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    private void logConnection(final HttpURLConnection connection) throws IOException, URISyntaxException {
        int code = connection.getResponseCode();
        String message = connection.getResponseMessage();
        String url = connection.getURL().toURI().toString();
        Log.d("CONNECTION", String.valueOf("RESPONSE CODE "+code+", MESSAGE"+message+",url "+url));


    }

}
