package com.nabeel.deloitte.clothstore.net;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;



import org.json.JSONException;
import org.json.JSONObject;



public class AsyncTaskHttpRequest extends AsyncTask<String, Void, Response> {

    private String url, data, methodtype;
    private AsyncTaskInterface taskCompliteListener;
    private boolean isToShowIndicator;

    public AsyncTaskHttpRequest(String url, String data, String methodtype, AsyncTaskInterface taskCompliteListener) {

        this.url = url;
        this.data = data;
        this.methodtype = methodtype;
        this.taskCompliteListener = taskCompliteListener;
        isToShowIndicator = true;
    }



    @Override
    protected Response doInBackground(String... params) {
        try {
            return new ServiceHelper().doHttpUrlConnectionAction(data, url, methodtype);
        } catch (Exception e) {
            try {

                Response response = new Response();
                response.setResponseMessage(e.getMessage());
                response.setSuccess(false);
                return  response;
            } catch (Exception e1) {

            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Response result)
    {
        try
        {



            /***
             *
             * call default listener after completion of AsyncTask
             *
             */

        taskCompliteListener.onTaskCompleted(result);
    }
    catch (Exception e)
    {
        e.printStackTrace();

    }


    }
}


