package com.nabeel.deloitte.clothstore.model;

import android.view.View;

import androidx.databinding.BaseObservable;

public class Product  extends BaseObservable {


  public final static String PRODUCT_ID = "productId";
  public final static String NAME = "name";
  public final static String CATEGORY = "category";
  public final static String PRICE = "price";
    public final static String STOCK = "stock";


  private int productId ;
  private int cartId ;
  private String name ;
  public String category ;
  private double price;
  private String oldPrice;
  private int stock;
  private int category_type ;
  public int isOutOfStock;



    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
        isOutOfStock = stock ==0 ?View.VISIBLE:View.GONE ;
    }

    public int getCategory_type() {
        return category_type;
    }

    public void setCategory_type(int category_type) {
        this.category_type = category_type;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }


}
