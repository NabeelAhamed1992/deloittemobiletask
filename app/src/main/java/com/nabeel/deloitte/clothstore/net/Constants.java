package com.nabeel.deloitte.clothstore.net;



public class Constants {






    public static final String REQUEST_TYPE_POST = "POST";
    public static final String REQUEST_TYPE_GET = "GET";
    public static final String REQUEST_TYPE_DELETE = "DELETE";

    // Key
    public static final String KEY_CONTENT_TYPE = "Content-Type";
    public static final String KEY_ACCEPT = "Accept";


    public static final String VALUE_CONTENT_TYPE = "application/json";


    public static final String API_LIST_ALL_PRODUCTS = "https://private-anon-5a307ec4fe-ddshop.apiary-mock.com/products";
    public static final String API_ADD_PRODUCT_CART = "https://private-anon-803e8c6377-ddshop.apiary-mock.com/cart";
    public static final String API_DELETE_PRODUCT_FROM_CART = "https://private-anon-803e8c6377-ddshop.apiary-mock.com/cart/";


    /**
     *
     *  product types
     *
     */

    public static final int PRODUCT_TYPE_WOMEN_FOOT_WEAR = 1;
    public static final int PRODUCT_TYPE_MEN_FOOT_WEAR = 2;
    public static final int PRODUCT_TYPE_WOMEN_CASUAL_WEAR = 3;
    public static final int PRODUCT_TYPE_MEN_CASUAL_WEAR = 4 ;
    public static final int PRODUCT_TYPE_WOMEN_FORMAL_WEAR = 5;
    public static final int PRODUCT_TYPE_MEN_FORMAL_WEAR = 6;
    public static final int PRODUCT_TYPE_ALL = 7;

    /**
     *
     *
     */

    public static final String PRODUCT_TYPE_WOMEN_FOOT_WEAR_STR = "women's footwear";
    public static final String PRODUCT_TYPE_MEN_FOOT_WEAR_STR = "men's footwear";
    public static final String PRODUCT_TYPE_WOMEN_CASUAL_WEAR_STR = "women's casualwear";
    public static final String PRODUCT_TYPE_MEN_CASUAL_WEAR_STR = "men's casualwear";
    public static final String PRODUCT_TYPE_WOMEN_FORMAL_WEAR_STR = "women's formalwear";
    public static final String PRODUCT_TYPE_MEN_FORMAL_WEAR_STR = "men's formalwear";

    public static final String SHOW_MSG_ALREADY_ADDED = "Already added";
    public static final String SHOW_MSG_MOVE_ITEM_CART_FAILED = "move item to cart failed please try again";
    public static final String SHOW_MSG_REMOVE_ITEM_FROM_CART_FAILED  =  "Remove item from cart failed please try again";
    public static final String SHOW_MSG_ADD_ITEM_TO_CART  = "Add item to cart failed please try again";
    public static final String SHOW_MSG_ADD_ITEM_TO_WISHLIST_FAILED  =  "Add item to wish list failed please try again";
    public static final String SHOW_MSG_FETCH_ITEM_FAILED= "fetching products failed please try again";
}

