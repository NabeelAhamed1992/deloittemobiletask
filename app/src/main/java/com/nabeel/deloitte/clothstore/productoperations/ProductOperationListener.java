package com.nabeel.deloitte.clothstore.productoperations;

import com.nabeel.deloitte.clothstore.model.Product;

import java.util.ArrayList;

public interface ProductOperationListener {

    void onProductRemovedFromCart(Product product);
    void onProductAddedToCart(Product product);
    void onProductAddedToWishList(Product product);
    void onFetchedAllProducts(ArrayList<Product> products);
    void onError(String message);
}
