package com.nabeel.deloitte.clothstore.net;

import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.productoperations.ProductOperations;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONParser {


    public static ArrayList<Product> parseListProdcuts(String json) throws JSONException
    {


            JSONArray jsonArray = new JSONArray(json);
            for(int i= 0;i<=jsonArray.length()-1;i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                Product product = new Product();
                product.setProductId(jsonObject.optInt(Product.PRODUCT_ID));
                product.setName(jsonObject.optString(Product.NAME));
                product.setCategory(jsonObject.optString(Product.CATEGORY));
                product.setPrice(jsonObject.optDouble(Product.PRICE));
                product.setStock(jsonObject.optInt(Product.STOCK));

                ProductOperations.getInstance().addProductToCategory(product);
            }

        return ProductOperations.getInstance().getProductCategory(Constants.PRODUCT_TYPE_ALL);
    }
}
