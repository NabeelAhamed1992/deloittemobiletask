package com.nabeel.deloitte.clothstore.productoperations;

import androidx.lifecycle.MutableLiveData;

import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.net.AsyncTaskInterface;
import com.nabeel.deloitte.clothstore.net.AsyncTaskHttpRequest;
import com.nabeel.deloitte.clothstore.net.Constants;
import com.nabeel.deloitte.clothstore.net.JSONParser;
import com.nabeel.deloitte.clothstore.net.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductOperations {
    private static final ProductOperations ourInstance = new ProductOperations();

    public static ProductOperations getInstance() {
        return ourInstance;
    }

    private ProductOperations() {
    }

    /**
     *
     * product repo
     *
     */

    private HashMap<Integer, Product> cartItems = new HashMap<>();
    private HashMap<Integer, Product> wishList = new HashMap<>();
    private HashMap<Integer, ArrayList<Product>> productCategories = new HashMap<>();


    /***
     * add product to cart
     *
     * @param product
     * @param productOperationListener
     */

   public void addToCart(final Product product, final ProductOperationListener productOperationListener)
    {
        JSONObject object = new JSONObject();
        try {
            object.put("productId", product.getProductId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(checkProductCanBeAddedToCart(product)) {


            AsyncTaskHttpRequest addToCartAsync = new AsyncTaskHttpRequest(Constants.API_ADD_PRODUCT_CART, object.toString(), Constants.REQUEST_TYPE_POST, new AsyncTaskInterface() {
                @Override
                public void onTaskCompleted(Response response) {

                    try {

                        if (response.isSuccess()) {
                            JSONObject responseObj = new JSONObject(response.getResponseStr());
                            int cartId = responseObj.optInt("cartId");
                            int productId = responseObj.optInt("productId");

                            product.setCartId(cartId);
                            cartItems.put(product.getProductId(), product);
                            productOperationListener.onProductAddedToCart(product);
                        } else {
                            productOperationListener.onError(response.getResponseMessage());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        productOperationListener.onError(e.getMessage());
                    }


                }
            });

            addToCartAsync.execute();
        }
        else
        {
            productOperationListener.onError(Constants.SHOW_MSG_ALREADY_ADDED);
        }



    }


    public void removeFromCart(final Product product, final ProductOperationListener operationListener)
    {


        String url = String.valueOf(Constants.API_DELETE_PRODUCT_FROM_CART+product.getCartId());



        AsyncTaskHttpRequest removeFromCartAsync = new AsyncTaskHttpRequest(url, "", Constants.REQUEST_TYPE_DELETE, new AsyncTaskInterface() {
            @Override
            public void onTaskCompleted(Response response) {

                try {


                    if(response.isSuccess())
                    {
                        if (response.getStatusCode() == HttpURLConnection.HTTP_NO_CONTENT) {
                            cartItems.remove(product.getProductId());
                            operationListener.onProductRemovedFromCart(product);
                        }
                    }
                    else
                    {
                        operationListener.onError(response.getResponseMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    operationListener.onError(e.getMessage());
                }


            }
        });

        removeFromCartAsync.execute();

    }
    public void addToWishList(Product product,ProductOperationListener listener)
    {
        if(checkProductCanBeAddedToWishList(product)) {
            wishList.put(product.getProductId(), product);
            listener.onProductAddedToWishList(product);

        }
        else
        {
            listener.onError(Constants.SHOW_MSG_ALREADY_ADDED);
        }

    }

    public void removeFromWishList(Product product)
    {
        wishList.remove(product.getProductId());
    }


    public ArrayList<Product> getCartItems()
    {
        ArrayList<Product> cartIemsList = new ArrayList<>();
        for (Map.Entry<Integer, Product> entry : cartItems.entrySet()) {
            cartIemsList.add(entry.getValue());

        }
        return cartIemsList;
    }

    public boolean checkProductCanBeAddedToWishList(Product product)
    {


            /**
             * check product already there
             */
            Product Checkproduct = wishList.get(product.getProductId());
            if(Checkproduct == null)
            {
                // cartItems.put(product.getProductId(),product);
                return true;
            }
            else
            {
                return false;
            }




    }
    public boolean checkProductCanBeAddedToCart(Product product)
    {
        if(product.getStock() > 0)
        {
            /**
             * check product already there
             */
            Product Checkproduct = cartItems.get(product.getProductId());
            if(Checkproduct == null)
            {
               // cartItems.put(product.getProductId(),product);
                return true;
            }
            else
            {
                return false;
            }


        }
        else
        {
            return false;
        }






    }
    public Double getTotalAmount()
    {


        Double totalCost = Double.valueOf(0);
        for(Product product :getCartItems())
        {
            totalCost= product.getPrice()+totalCost;

        }
        return totalCost;


    }

    public ArrayList<Product> getWishListItems()
    {
        ArrayList<Product> cartIemsList = new ArrayList<>();
        for (Map.Entry<Integer, Product> entry : wishList.entrySet()) {
            cartIemsList.add(entry.getValue());

        }
        return cartIemsList;
    }






    public void addProductToCategory(Product product)
    {


        classifyProduct(product);

        ArrayList<Product> productsType = productCategories.get(product.getCategory_type());
        if(productsType!=null)
        {
            productsType.add(product);
            productCategories.put(product.getCategory_type(),productsType);
        }
        else
        {
            productsType = new ArrayList<>();
            productsType.add(product);
            productCategories.put(product.getCategory_type(),productsType);

        }
    }



    public ArrayList<Product> getProductCategory(int type)
    {
        if(type == Constants.PRODUCT_TYPE_ALL) return getAllProducts();

        return productCategories.get(type);
    }
    public ArrayList<Product> getAllProducts()
    {
        ArrayList<Product> allProducts = new ArrayList<>();
        for (Map.Entry<Integer, ArrayList<Product>> entry : productCategories.entrySet()) {
            allProducts.addAll(entry.getValue());

        }
        return allProducts;
    }

    private void classifyProduct(Product product)
    {
        String typeStr = product.getCategory().toLowerCase();
        switch (typeStr)
        {
            case Constants.PRODUCT_TYPE_WOMEN_FOOT_WEAR_STR:
                product.setCategory_type(Constants.PRODUCT_TYPE_WOMEN_FOOT_WEAR);
                break;
            case Constants.PRODUCT_TYPE_MEN_FOOT_WEAR_STR:
                product.setCategory_type(Constants.PRODUCT_TYPE_MEN_FOOT_WEAR);
                break;
            case Constants.PRODUCT_TYPE_WOMEN_CASUAL_WEAR_STR:
                product.setCategory_type(Constants.PRODUCT_TYPE_WOMEN_CASUAL_WEAR);
                break;
            case Constants.PRODUCT_TYPE_MEN_CASUAL_WEAR_STR:
                product.setCategory_type(Constants.PRODUCT_TYPE_MEN_CASUAL_WEAR);
                break;
            case Constants.PRODUCT_TYPE_MEN_FORMAL_WEAR_STR:
                product.setCategory_type(Constants.PRODUCT_TYPE_MEN_FORMAL_WEAR);
                break;
            case Constants.PRODUCT_TYPE_WOMEN_FORMAL_WEAR_STR:
                product.setCategory_type(Constants.PRODUCT_TYPE_WOMEN_FORMAL_WEAR);
                break;
        }
    }





    public void fetchList(final ProductOperationListener productOperationListener) {

        final AsyncTaskHttpRequest fetchProductAsync = new AsyncTaskHttpRequest(Constants.API_LIST_ALL_PRODUCTS, "", Constants.REQUEST_TYPE_GET, new AsyncTaskInterface() {
            @Override
            public void onTaskCompleted(Response response) {

                if(response.isSuccess())
                {
                    ArrayList<Product> productArrayList = null;
                    try {
                        productArrayList = JSONParser.parseListProdcuts(response.getResponseStr());
                        productOperationListener.onFetchedAllProducts(productArrayList);
                    } catch (Exception e) {
                        e.printStackTrace();
                        productOperationListener.onError(e.getMessage());
                    }

                }
                else
                {
                    productOperationListener.onError(response.getResponseMessage());
                }





            }
        });

        fetchProductAsync.execute();



    }

    public HashMap<Integer, Product> getCartRepo()
    {
        return this.cartItems;
    }
    public HashMap<Integer, Product> getWishRepo()
    {
        return this.wishList;
    }



}
