package com.nabeel.deloitte.clothstore;

import com.nabeel.deloitte.clothstore.model.Product;
import com.nabeel.deloitte.clothstore.net.Constants;
import com.nabeel.deloitte.clothstore.productoperations.ProductOperations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ProductOperationUnitTest {


    @Test
    public  void checkForCategory()
    {
        Product product = new Product();
        product.setStock(4);

        product.setProductId(1);
        product.setCategory("Men's Footwear");
        product.setName("shoe");

        ProductOperations.getInstance().addProductToCategory(product);


        Product product1 = new Product();
        product1.setStock(4);

        product1.setProductId(2);
        product1.setCategory("Women's Footwear");
        product1.setName("sandals");

        ProductOperations.getInstance().addProductToCategory(product1);



        assertEquals(1,ProductOperations.getInstance().getProductCategory(Constants.PRODUCT_TYPE_MEN_FOOT_WEAR).size());
        assertEquals(1,ProductOperations.getInstance().getProductCategory(Constants.PRODUCT_TYPE_WOMEN_FOOT_WEAR).size());
        assertEquals(2,ProductOperations.getInstance().getAllProducts().size());

    }

    /***
     *
     * only product with stock value above 0 can be added
     *
     */

    @Test
    public void checkProductCanBeAddedOnlyIfStockAvailable()
    {
        Product product = new Product();
        product.setStock(0);
        product.setProductId(1);
        product.setCategory("Men's Footwear");
        product.setName("shoe");

        assertEquals(false,ProductOperations.getInstance().checkProductCanBeAddedToCart(product));



    }



    @Test
    public void checkProductCanBeAddedIfAlreadyExist()
    {
        final Product sproduct = new Product();
        sproduct.setStock(1);
        sproduct.setProductId(1);
        sproduct.setCategory("Men's Footwear");
        sproduct.setName("shoe");
        ProductOperations.getInstance().getCartRepo().put(sproduct.getProductId(), sproduct);



        assertEquals(false,ProductOperations.getInstance().checkProductCanBeAddedToCart(sproduct));



    }

    @Test
    public void checkProductCanBeAddedIfAlreadyExistInWishList()
    {
        final Product sproduct = new Product();
        sproduct.setStock(1);
        sproduct.setProductId(1);
        sproduct.setCategory("Men's Footwear");
        sproduct.setName("shoe");
        ProductOperations.getInstance().getWishRepo().put(sproduct.getProductId(), sproduct);



        assertEquals(false,ProductOperations.getInstance().checkProductCanBeAddedToWishList(sproduct));



    }


    @Test
    public void checkProductTotalCostCalulcation()
    {
        final Product sproduct = new Product();
        sproduct.setStock(1);
        sproduct.setProductId(1);
        sproduct.setCategory("Men's Footwear");
        sproduct.setName("shoe");
        sproduct.setPrice(100);
        ProductOperations.getInstance().getCartRepo().put(sproduct.getProductId(), sproduct);

        final Product sproduct1 = new Product();
        sproduct1.setStock(1);
        sproduct1.setProductId(2);
        sproduct1.setCategory("Men's Footwear");
        sproduct1.setName("shoe");
        sproduct1.setPrice(200);
        ProductOperations.getInstance().getCartRepo().put(sproduct1.getProductId(), sproduct1);
        Double totalValue = Double.valueOf(300);

        assertEquals(totalValue,ProductOperations.getInstance().getTotalAmount());






    }

}